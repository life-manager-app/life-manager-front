# Goals

# Dashboard

Search in project "#issue" and fix/understand it

---


# todo - research CommonModule (!)

#todo - write full algorithm

1. create Feature state
2. create actions (enum)
3. create Action creators

class SomeAction implements Action {
readonly type = ActionEnumTypes.SomeAction
  
 constructor(public payload: <Type>) {}
}

---

## Todo

-   prettier
-   linting
    -- tslint
    -- jslint
    -- stylelint
-   lazy-loading don't work
-   init gitlab repo with .gitignore

---

TODO learn modules: 'es2015' - default, but don't work import in loadChildren in "app-routing.ts" lazy loading  
// "module": "es2015",
"module": "commonjs",

---

---

@Injectable({
providedIn: TasksListModule // todo check how it work
})

---

# NGRX NOTES

1. install all packages: store, effects, store-devtools(-D) ...
2. import ngrx modules in root module (app.module)

```
    ...
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument(
        {
            maxAge: 25,
            name: 'lima',
            logOnly: environment.production
        }
    )
    ...
```

3. import ngrx modules in feature module

```
    ...
    StoreModule.forFeature('tasksList', tasksListReducer),
    EffectsModule.forFeature([TasksListEffects])
    ...
```

todo: (feature name 'tasksList' need for register(?) feature-store and create feature-selector,
but if create `const featureName = 'featureName'` - circular dependency)

-   look how it resolve in other project (Vova Konoplev)

4. create folder structure: (todo - need approve!)
   in feature dir: - models - services - ... - store
   -- feature.actions.ts
   -- feature.effects.ts
   -- feature.reducer.ts
   -- feature.selectors.ts

## Actions

`feature.actions.ts` (source = https://www.youtube.com/watch?v=5JzOlB5eNDA&feature=share)

### Action

---

```
    export enum FeatureActionTypes {
        SomeAction = '[Feature] Some Action',
    }
```

or (первый вариант выглядит по интереснее)

```
    export const SomeAction = '[Feature] Some Action';
```

### Action creators

---

```
    export class SomeAction implements Action {
        readonly type = FeatureActionTypes.SomeAction;

        constructor(public payload: IValue) {}
    }
```

usage: `new SomeAction(payload);`

### Action types

---

-   This type use in reducer. (todo - only?)
-   It is union of action creators.

```
    export type FeatureAction = SomeAction
        | ...
        | ...;
```

## Reducer

`feature.reducer.ts` contains:

-   featureState interface
-   initialState constants
-   reducer

```
    export interface FeatureState {
        someValue: IValue;
        ...
    }
```

```
    const initialState: FeatureState = {
        someValue: null,
        ...
    };
```

-   todo: AppState - main app state? Do I really need him? And for what?

```
    export interface State extends AppState {
        featureState: FeatureState;
    }
```

```
export function reducer(state = initialState, action: FeatureActions): FeatureState {
    switch (action.type) {
        # todo FeatureActionTypes
        case FeatureActionTypes.SomeAction:
            return {
                ...state,
                someValue: action.payload
            };

        default:
            return state;
    }
}
```

## Selectors

7. `feature.selectors.ts`

-   createFeatureSelector - generic. type: FeatureState
-   don`t work `const featureName ='feature';` - circular dependency - todo!

```
    // from feature.reducer.ts
    export interface FeatureState {
        someValue: IValue;
        ...
    }
```

```
    const getFeatureNameFeatureState = createFeatureSelector<FeatureState>('featureName');
```

-   target(?) selectors - pure functions

```
    export const getFeatureSomeValue= createSelector(
        getFeatureNameFeatureState,
        state => state.someValue
    );
```

## Effects

8. `feature.effects.ts`

-   todo

*   fact: we can process actions both in the reducer and in the effects
    for example:

`in reducer:`

```
    case(FeatureActionTypes.Load):
        return {
            ... state,
            loading: true
        }
    case(FeatureActionTypes.LoadSuccess):
        return {
            ... state,
            loading: false
        }
```

`in effects:`

```
    @Effect()
    load$ = this.action$.pipe(
        ofType(TasksListActionTypes.LoadTasks),
        switchMap((action: LoadTasks) => ... )
    )
```

Effects:

1. in `feature.module`
   import `EffectsModule.forFeature([])`

2. create effect service: `feature.effects.ts`

```
@Injectable()
export class TasksListEffects {

    constructor(private action$: Actions) {}
}
```

effect - decorated field of this service.
This field - observable, which 'pipes' the action$ field (this.action$.pipe())
Make some async act, and, as result of act, 'dispatch' new actions

```
    @Effect()
    load$ = this.action$.pipe(
        ofType(FeatureActionTypes.Load),
        switchMap((action: Load) =>
            this.featureApiService.getItem$()
                .pipe(
                    map((items: ItemDto[]) => new LoadSuccess(items)),
                    catchError(error => of(new LoadFail(error)))
                )
        )
    )
```

## Router state

-   npm install @ngrx/router-store -S

#### todo

@Effect({dispatch: false})

## NGRX Tests

9. tests - TODO

todo - ProductsState (AppState), RecudersMAp -
[source | https://youtu.be/uTCBTdrHWNw?t=480]

------------ implementation TaskDetailsStore

1. add new feature in StoreFeatures in app.store.ts
2. add interface TaskDetailsState in task-details.reducer.ts
3. create `const initialState` in task-details.reducer.ts
4. create `interface State extends AppState` in task-details.reducer.ts
5. create `function taskDetailsReducer()` in task-details.reducer.ts
6. create task-details-store module and register taskDetailsReducer in store
7. create `enum TaskDetailsActionTypes` (actions) in task-details.actions.ts
8. create some action creators in task-details.actions.ts
9. create `type TaskAction` in task-details.actions.ts
   9.1) use `type TaskAction` for typing reducer argument in task-details.reducer.ts
10. inject `private store: Store<State>` in TaskDetailsPageComponent
11. dispatch by store `new LoadTask('taskId')` action and update LoadTask action creator
12. init TaskDetailsEffects in task-details.effects.ts and register it in TaskDetailsStoreModule
13. init TaskDetailsApiService and provide it in TaskDetailsModule
14. create method #getTaskDetails\$ (stub) in TaskDetailsApiService
15. create effect `loadTask$`
16. create file `task-details.selectors`
17. create `getTaskDetailsFeatureState` selector in task-details.selectors.ts
