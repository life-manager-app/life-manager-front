export type ISODateString = string;

export interface ServerError {
    message?: string;
}
