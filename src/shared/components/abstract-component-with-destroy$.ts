import { OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export class AbstractComponentWithDestroy$ implements OnDestroy {
    private _destroy$ = new Subject<void>();

    get destroy$(): Observable<void> {
        return this._destroy$.asObservable();
    }

    ngOnDestroy() {
        this._destroy$.next();
        this._destroy$.complete();
    }
}
