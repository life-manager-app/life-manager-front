import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { TaskDetailsDto } from '../models/task-details-dto';

@Injectable()
export class TaskDetailsApiService {
    getTaskDetails$(taskId: string): Observable<TaskDetailsDto> {
        return of({id: taskId});
    }
}
