import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskDetailsPageComponent } from './task-details-page.component';

@NgModule({
  declarations: [TaskDetailsPageComponent],
  imports: [
    CommonModule
  ]
})
export class TaskDetailsPageModule { }
