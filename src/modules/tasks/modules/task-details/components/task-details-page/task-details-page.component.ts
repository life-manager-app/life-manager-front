import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../../store/task-details.reducer';
import { LoadTask } from '../../store/task-details.actions';

@Component({
  selector: 'lima-task-details-page',
  templateUrl: './task-details-page.component.html',
  styleUrls: ['./task-details-page.component.scss']
})
export class TaskDetailsPageComponent implements OnInit {

  constructor(private store: Store<State>) { }

  ngOnInit() {
      const taskId = 'taskId'; // need take it from routerState

      this.store.dispatch(new LoadTask(taskId));
  }

}
