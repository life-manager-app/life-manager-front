import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskDetailsPageModule } from './components/task-details-page/task-details-page.module';
import { TaskDetailsApiService } from './services/task-details-api.service';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        TaskDetailsPageModule
    ],
    providers: [TaskDetailsApiService]
})
export class TaskDetailsModule { }
