import { Action } from '@ngrx/store';
import { TaskDetailsDto } from '../models/task-details-dto';

export enum TaskDetailsActionTypes {
    LoadTask = '[Task Details] Load Task Details',
    LoadTaskSuccess = '[Task Details] Load Task Details Success',
    LoadTaskFail = '[Task Details] Load Task Details Fail'
}

export class LoadTask implements Action {
    readonly type = TaskDetailsActionTypes.LoadTask;

    constructor(public payload: string) {}
}

export class LoadTaskSuccess implements Action {
    readonly type = TaskDetailsActionTypes.LoadTaskSuccess;

    constructor(public payload: TaskDetailsDto) {}
}

export class LoadTaskFail implements Action {
    readonly type = TaskDetailsActionTypes.LoadTaskFail;

    constructor(public payload: any) {}
}

export type TaskAction = LoadTask
    | LoadTaskSuccess
    | LoadTaskFail;

