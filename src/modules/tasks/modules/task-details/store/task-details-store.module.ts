import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreFeatures } from '../../../../app/store/app.store';
import { taskDetailsReducer } from './task-details.reducer';
import { TaskDetailsEffects } from './task-details.effects';

@NgModule({
    imports: [
        StoreModule.forFeature(StoreFeatures.TaskDetails, taskDetailsReducer),
        EffectsModule.forFeature([TaskDetailsEffects])
    ]
})
export class TaskDetailsStoreModule { }
