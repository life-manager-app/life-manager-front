import { TaskDetailsDto } from '../models/task-details-dto';
import { AppState, StoreFeatures } from '../../../../app/store/app.store';
import { TaskAction } from './task-details.actions';

export interface TaskDetailsState {
    task: TaskDetailsDto,
    loading: boolean,
    loadingError: any | null
}

const initialState: TaskDetailsState = {
    task: null,
    loading: false,
    loadingError: null
};

export interface State extends AppState {
    [StoreFeatures.TaskDetails]: TaskDetailsState;
}

export function taskDetailsReducer(state = initialState, action: TaskAction): TaskDetailsState {
    return state;
}
