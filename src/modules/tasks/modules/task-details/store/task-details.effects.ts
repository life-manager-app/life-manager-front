import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { LoadTask, LoadTaskFail, LoadTaskSuccess, TaskDetailsActionTypes } from './task-details.actions';
import { TaskDetailsApiService } from '../services/task-details-api.service';

@Injectable()
export class TaskDetailsEffects {
    constructor(private action$: Actions, private apiService: TaskDetailsApiService) {}

    @Effect()
    loadTask$ = this.action$
        .pipe(
            ofType(TaskDetailsActionTypes.LoadTask),
            switchMap((action: LoadTask) =>
                this.apiService.getTaskDetails$(action.payload)
            ),
            map(task => new LoadTaskSuccess(task)),
            catchError(error => of(new LoadTaskFail(error)))
        )
}
