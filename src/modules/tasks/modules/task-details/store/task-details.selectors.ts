import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StoreFeatures } from '../../../../app/store/app.store';
import { TaskDetailsState } from './task-details.reducer';

const getTaskDetailsFeatureState = createFeatureSelector<TaskDetailsState>(StoreFeatures.TaskDetails);

const getTaskDetails = createSelector(
    getTaskDetailsFeatureState,
    (state: TaskDetailsState) => state.task
);
