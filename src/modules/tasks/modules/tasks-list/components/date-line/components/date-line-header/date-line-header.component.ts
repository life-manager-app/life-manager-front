import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'lima-date-line-header',
    templateUrl: './date-line-header.component.html',
    styleUrls: ['./date-line-header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateLineHeaderComponent {}


/**
 *
 * todo
 *
 * Если слева | справа начинается другой месяц, сделать серым пропадающим градиентом
 *
 */
