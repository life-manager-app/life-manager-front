import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateLineHeaderComponent } from './date-line-header.component';

@NgModule({
    declarations: [DateLineHeaderComponent],
    imports: [CommonModule],
    exports: [DateLineHeaderComponent]
})
export class DateLineHeaderModule {}
