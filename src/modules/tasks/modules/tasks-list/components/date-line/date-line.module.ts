import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateLineComponent } from './date-line.component';
import { DateLineHeaderModule } from './components/date-line-header/date-line-header.module';

@NgModule({
    imports: [CommonModule, DateLineHeaderModule],
    declarations: [DateLineComponent],
    exports: [DateLineComponent]
})
export class DateLineModule {}
