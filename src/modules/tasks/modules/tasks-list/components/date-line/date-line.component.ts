import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DaysAbbreviation } from 'modules/tasks/modules/create-task/models/day';

@Component({
    selector: 'lima-date-line',
    templateUrl: './date-line.component.html',
    styleUrls: ['./date-line.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DateLineComponent {
    daysAbbreviation = DaysAbbreviation;
}
