import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressIconComponent } from './progress-icon.component';

@NgModule({
  declarations: [ProgressIconComponent],
  imports: [
    CommonModule
  ],
    exports: [ProgressIconComponent]
})
export class ProgressIconModule { }
