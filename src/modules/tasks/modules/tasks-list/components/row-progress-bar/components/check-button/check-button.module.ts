import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckButtonComponent } from './check-button.component';

@NgModule({
    declarations: [CheckButtonComponent],
    imports: [CommonModule],
    exports: [CheckButtonComponent]
})
export class CheckButtonModule {}
