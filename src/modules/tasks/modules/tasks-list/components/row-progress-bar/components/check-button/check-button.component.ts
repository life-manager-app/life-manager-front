import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'lima-check-button',
    templateUrl: './check-button.component.html',
    styleUrls: ['./check-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckButtonComponent {
    @Input() taskDone = false;
}

/**
 * todo
 *      Заботать SVG - как делать гибкий размер
 */
