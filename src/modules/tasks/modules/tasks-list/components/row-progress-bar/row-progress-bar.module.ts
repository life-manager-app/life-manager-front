import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RowProgressBarComponent } from './row-progress-bar.component';
import { CheckButtonModule } from './components/check-button/check-button.module';

@NgModule({
    declarations: [RowProgressBarComponent],
    imports: [CommonModule, CheckButtonModule],
    exports: [RowProgressBarComponent]
})
export class RowProgressBarModule {}
