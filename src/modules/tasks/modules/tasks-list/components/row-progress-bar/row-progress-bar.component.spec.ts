import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RowProgressBarComponent } from './row-progress-bar.component';

describe('RowProgressBarComponent', () => {
  let component: RowProgressBarComponent;
  let fixture: ComponentFixture<RowProgressBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RowProgressBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RowProgressBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
