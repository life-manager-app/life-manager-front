import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'lima-tasks-list-item',
    templateUrl: './tasks-list-item.component.html',
    styleUrls: ['./tasks-list-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksListItemComponent {}
