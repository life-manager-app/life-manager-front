import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RowProgressBarModule } from 'modules/tasks/modules/tasks-list/components/row-progress-bar/row-progress-bar.module';
import { TasksListItemComponent } from './tasks-list-item.component';

@NgModule({
    declarations: [TasksListItemComponent],
    imports: [CommonModule, RowProgressBarModule],
    exports: [TasksListItemComponent]
})
export class TasksListItemModule {}
