import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { BehaviorSubject, Observable } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { AbstractComponentWithDestroy$ } from 'shared/components';
import { TasksListItemDto } from 'modules/tasks/modules/tasks-list/models/tasks-list-item.dto';
import { LoadTasks } from '../../store/tasks-list.actions';
import {
    getTasksListItems,
    getTasksListLoading
} from '../../store/tasks-list.selectors';
import { State } from '../../store/tasks-list.reducer';

@Component({
    selector: 'lima-tasks-list-page',
    templateUrl: './tasks-list-page.component.html',
    styleUrls: ['./tasks-list-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TasksListPageComponent extends AbstractComponentWithDestroy$
    implements OnInit {
    constructor(private store: Store<State>) {
        super();
    }

    loading$: Observable<boolean>;
    item$: Observable<TasksListItemDto[]>;

    ngOnInit() {
        // todo - попробовать в резолвере доставать таски?
        this.store.dispatch(new LoadTasks());

        this.loading$ = this.store.pipe(
            select(getTasksListLoading),
            takeUntil(this.destroy$)
        );

        this.item$ = this.store.pipe(
            select(getTasksListItems),
            takeUntil(this.destroy$)
        );
    }
}
