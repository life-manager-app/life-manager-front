import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksListPageComponent } from './tasks-list-page.component';
import { DateLineModule } from '../date-line/date-line.module';
import { TasksListItemModule } from '../tasks-list-item/tasks-list-item.module';

@NgModule({
    imports: [CommonModule, TasksListItemModule, DateLineModule],
    declarations: [TasksListPageComponent],
    exports: [TasksListPageComponent]
})
export class TasksListPageModule {}
