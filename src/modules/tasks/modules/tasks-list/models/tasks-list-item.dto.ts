export class TasksListItemDto {
    id: string;
    title: string;
    description: string;
}
