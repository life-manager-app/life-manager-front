import { TasksListItemDto } from '../models/tasks-list-item.dto';

export const TasksListItemDtoMock: TasksListItemDto[] = [
    {
        id: 'some-id-123',
        title: 'title 1',
        description: 'description 1'
    },
    {
        id: 'asdasdasd',
        title: 'title 123',
        description: 'descr lorem'
    },
    {
        id: 'kekus',
        title: 'ne title',
        description: 'description 1'
    },
    {
        id: '123-asd-09-asd-3',
        title: 'atricle',
        description: 'service lol 1'
    }
];
