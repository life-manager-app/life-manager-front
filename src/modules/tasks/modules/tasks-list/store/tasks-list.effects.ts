import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import {
    LoadTasksFail,
    LoadTasksSuccess,
    TasksListActionTypes
} from './tasks-list.actions';
import { TasksListApiService } from '../services/tasks-list-api.service';
import { TasksListItemDto } from '../models/tasks-list-item.dto';

@Injectable()
export class TasksListEffects {
    constructor(
        private action$: Actions,
        private tasksListApiService: TasksListApiService
    ) {}

    @Effect()
    loadTask$ = this.action$.pipe(
        ofType(TasksListActionTypes.LoadTasks),
        switchMap(() => this.tasksListApiService.getTasksListItem$()),
        map((tasks: TasksListItemDto[]) => new LoadTasksSuccess(tasks)),
        catchError(error => of(new LoadTasksFail(error)))
    );
}
