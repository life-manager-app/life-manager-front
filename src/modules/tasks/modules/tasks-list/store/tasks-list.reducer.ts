import { TasksListItemDto } from '../models/tasks-list-item.dto';
import { TasksListAction, TasksListActionTypes } from './tasks-list.actions';
import { AppState, StoreFeatures } from '../../../../app/store/app.store';

export interface TasksListState {
    tasks: TasksListItemDto[],
    loading: boolean,
    loadingError: any | null
}

const initialState: TasksListState = {
    tasks: [],
    loading: false,
    loadingError: null
};


// todo - заботать, где этот тип может пригодиться, помимо 'private store: Store<State>'
// todo - see app.store.ts and create notes and приемущества над интерфейсами
// почему то все делают интерфесы, хотя тут можно было бы стильно запилить тип (как я)
export interface State extends AppState {
    // todo - this key - feature name
    [StoreFeatures.TasksList]: TasksListState;

}


export function tasksListReducer(state = initialState, action: TasksListAction): TasksListState {
    switch (action.type) {
        case TasksListActionTypes.LoadTasks:
            return {
                ...state,
                loading: true
            };
        case TasksListActionTypes.LoadTasksSuccess:
            return {
                ...state,
                tasks: action.payload,
                loading: false
            };
        case TasksListActionTypes.LoadTasksFail:
            return {
                ...state,
                loadingError: action.payload,
                loading: false
            };

        default:
            return state;
    }
}
