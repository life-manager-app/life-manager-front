import { Action } from '@ngrx/store';

export enum TasksListActionTypes {
    LoadTasks = '[Tasks List] Load Tasks List',
    LoadTasksSuccess = '[Tasks List] Load Tasks List Success',
    LoadTasksFail = '[Tasks List] Load Tasks List Fail'
}

export class LoadTasks implements Action {
    readonly type = TasksListActionTypes.LoadTasks;
}

export class LoadTasksSuccess implements Action {
    readonly type = TasksListActionTypes.LoadTasksSuccess;

    constructor(public payload: any) {}
}

export class LoadTasksFail implements Action {
    readonly type = TasksListActionTypes.LoadTasksFail;

    constructor(public payload: any) {}
}

export type TasksListAction = LoadTasks | LoadTasksSuccess | LoadTasksFail;
