import { createFeatureSelector, createSelector } from '@ngrx/store';
import { StoreFeatures } from 'modules/app/store/app.store';
import { TasksListState } from './tasks-list.reducer';

const getTasksListFeatureState = createFeatureSelector<TasksListState>(
    StoreFeatures.TasksList
);

export const getTasksListItems = createSelector(
    getTasksListFeatureState,
    state => state.tasks
);

export const getTasksListLoading = createSelector(
    getTasksListFeatureState,
    state => state.loading
);
