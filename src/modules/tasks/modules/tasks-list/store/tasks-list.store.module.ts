import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { StoreFeatures } from 'modules/app/store/app.store';
import { tasksListReducer } from 'modules/tasks/modules/tasks-list/store/tasks-list.reducer';
import { TasksListEffects } from 'modules/tasks/modules/tasks-list/store/tasks-list.effects';

@NgModule({
    imports: [
        StoreModule.forFeature(StoreFeatures.TasksList, tasksListReducer),
        EffectsModule.forFeature([TasksListEffects])
    ]
})
export class TasksListStoreModule {}
