import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { TasksListModule } from '../tasks-list.module';
import { TasksListItemDto } from '../models/tasks-list-item.dto';
import { TasksListItemDtoMock } from '../tests/tasks-list-item.dto.mock';

@Injectable({
    // providedIn: TasksListModule  // todo check how it work
    providedIn: 'root'
})
export class TasksListApiService {
    constructor() {}

    // api request
    getTasksListItem$(): Observable<TasksListItemDto[]> {
        return of(TasksListItemDtoMock).pipe(delay(300));
    }
}
