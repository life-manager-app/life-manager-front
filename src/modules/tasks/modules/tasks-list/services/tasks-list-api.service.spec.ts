import { TestBed } from '@angular/core/testing';

import { TasksListApiService } from './tasks-list-api.service';

describe('TasksListApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TasksListApiService = TestBed.get(TasksListApiService);
    expect(service).toBeTruthy();
  });
});
