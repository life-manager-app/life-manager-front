import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksListPageModule } from './components/tasks-list-page/tasks-list-page.module';
import { TasksListStoreModule } from 'modules/tasks/modules/tasks-list/store/tasks-list.store.module';

@NgModule({
    imports: [CommonModule, TasksListPageModule, TasksListStoreModule]
})
export class TasksListModule {}
