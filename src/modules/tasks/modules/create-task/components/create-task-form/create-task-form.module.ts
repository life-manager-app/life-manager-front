import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateTaskFormComponent } from './create-task-form.component';
import { DaysPickerModule } from './components/days-picker/days-picker.module';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule, DaysPickerModule],
    declarations: [CreateTaskFormComponent],
    exports: [CreateTaskFormComponent]
})
export class CreateTaskFormModule {}
