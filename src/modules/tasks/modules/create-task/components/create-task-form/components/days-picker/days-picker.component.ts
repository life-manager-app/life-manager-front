import { Component, Input } from '@angular/core';
import { ImplementationDaysForm } from '../../forms/implementation-days.form';

/**
 * #issue
 * todo: with changeDetection: ChangeDetectionStrategy.OnPush
 * when click on submit and call form.markAllAsTouched()
 * component does not call #showError in template and don't show error message
 * ----------
 *      Понять, что будет, если в листьях дерева компонентов
 * будет компонент без "ChangeDetectionStrategy.OnPush"
 *
 *      Как часто он будет ререндериться,
 * как на него будет влиять markForCheck(), detectChanges()
 */

@Component({
    selector: 'lima-days-picker',
    templateUrl: './days-picker.component.html',
    styleUrls: ['./days-picker.component.scss']
})
export class DaysPickerComponent {
    @Input() form: ImplementationDaysForm;

    // todo approve text #issue
    readonly errorMessage = 'Select at least one day';

    get showError(): boolean {
        return this.form.showOneOrMoreRequiredTrueError();
    }
}
