import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroupDirective } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DaysPickerComponent } from './days-picker.component';
import { DaysPickerModule } from './days-picker.module';
import { ImplementationDaysForm } from '../../forms/implementation-days.form';

describe(`${DaysPickerComponent.name} | `, () => {
    let component: DaysPickerComponent;
    let fixture: ComponentFixture<DaysPickerComponent>;

    describe('Unit | ', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DaysPickerModule]
            }).overrideTemplate(DaysPickerComponent, '');
        });

        beforeEach(() => {
            fixture = TestBed.createComponent(DaysPickerComponent);
            component = fixture.componentInstance;
            component.form = new ImplementationDaysForm();
        });

        it('#errorMessage | Should contains "Select at least one day" text', () => {
            expect(component.errorMessage).toBe('Select at least one day');
        });

        describe('#showError', () => {
            it('Should return "true", if form has error', () => {
                // arrange
                spyOn(
                    component.form,
                    'showOneOrMoreRequiredTrueError'
                ).and.returnValue(true);

                // assert
                expect(component.showError).toBe(true);
            });

            it('Should return "false", if form does not have error', () => {
                // arrange
                spyOn(
                    component.form,
                    'showOneOrMoreRequiredTrueError'
                ).and.returnValue(false);

                // assert
                expect(component.showError).toBe(false);
            });
        });
    });

    describe('Integration | ', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [DaysPickerModule]
            });
        });

        beforeEach(() => {
            fixture = TestBed.createComponent(DaysPickerComponent);
            component = fixture.componentInstance;
            component.form = new ImplementationDaysForm();

            fixture.detectChanges();
        });

        describe('Error section | ', () => {
            it('Should show validation error, if form has error', () => {
                // todo: why don't work? #issue
                // Object.defineProperty(component, 'showError', { get: () => true });

                // arrange
                spyOn(
                    component.form,
                    'showOneOrMoreRequiredTrueError'
                ).and.returnValue(true);

                // act
                fixture.detectChanges();

                const errorText = fixture.debugElement
                    .query(By.css('.error'))
                    .nativeElement.textContent.trim();

                // assert
                expect(errorText).toBe(component.errorMessage);
            });

            it('Should not show validation error, if form does not have error', () => {
                // arrange
                spyOn(
                    component.form,
                    'showOneOrMoreRequiredTrueError'
                ).and.returnValue(false);

                // act
                fixture.detectChanges();

                const error = fixture.debugElement.query(By.css('.error'));

                // assert
                expect(error).toBe(null);
            });
        });

        describe('Form | ', () => {
            it('Should insert form into template', () => {
                // act
                const { form } = fixture.debugElement
                    .query(By.css('form'))
                    .injector.get(FormGroupDirective) as any;

                // assert
                expect(form).toBe(component.form.get('single'));
            });

            it('Should insert controls into template ', () => {
                // act
                const controls = fixture.debugElement
                    .queryAll(By.css('input'))
                    .map(el => el.attributes['formControlName']);

                // assert
                // todo: #issue
                expect(controls).toEqual([
                    'monday',
                    'tuesday',
                    'wednesday',
                    'thursday',
                    'friday',
                    'saturday',
                    'sunday'
                ]);
            });
        });
    });
});
