import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DaysPickerComponent } from './days-picker.component';

@NgModule({
    imports: [CommonModule, FormsModule, ReactiveFormsModule],
    declarations: [DaysPickerComponent],
    exports: [DaysPickerComponent]
})
export class DaysPickerModule {}
