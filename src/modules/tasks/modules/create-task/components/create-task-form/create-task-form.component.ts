import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { AbstractComponentWithDestroy$ } from 'shared/components';
import { State } from 'modules/tasks/modules/create-task/store/create-task.reducer';
import { CreateTask } from 'modules/tasks/modules/create-task/store/create-task.actions';
import { CreateTaskFormConverterService } from 'modules/tasks/modules/create-task/services/create-task-form-converter.service';
import { CreateTaskForm } from './forms/create-task.form';

@Component({
    selector: 'lima-create-task-form',
    templateUrl: './create-task-form.component.html',
    styleUrls: ['./create-task-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTaskFormComponent extends AbstractComponentWithDestroy$ {
    // todo - typing this form (like annotatedForm from work)
    form = new CreateTaskForm();

    readonly titleError = 'Title error';

    constructor(
        private readonly store: Store<State>,
        private formConverter: CreateTaskFormConverterService
    ) {
        super();
    }

    submit() {
        this.form.markAllAsTouched();

        if (this.form.valid) {
            const dto = this.formConverter.convertFormToDto(this.form.value);

            this.store.dispatch(new CreateTask(dto));
        }
    }
}
