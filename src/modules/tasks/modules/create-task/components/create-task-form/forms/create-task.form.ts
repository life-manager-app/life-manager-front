import {
    AbstractControl,
    FormControl,
    FormGroup,
    Validators
} from '@angular/forms';
import { ImplementationDaysForm } from './implementation-days.form';

export class CreateTaskForm extends FormGroup {
    constructor() {
        const controls: { [controlName: string]: AbstractControl } = {
            title: new FormControl(null, Validators.required),
            description: new FormControl(null),
            implementationDays: new ImplementationDaysForm(),
            deadline: new FormControl(null)
        };

        super(controls);
    }

    hasTitleError(): boolean {
        return this.get('title').hasError('required');
    }

    showTitleError(): boolean {
        return this.get('title').touched && this.hasTitleError();
    }
}
