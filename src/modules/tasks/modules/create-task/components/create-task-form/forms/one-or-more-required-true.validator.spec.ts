import { FormControl, FormGroup } from '@angular/forms';
import {
    ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR,
    oneOrMoreRequiredTrueValidator
} from './one-or-more-required-true.validator';

describe(`${oneOrMoreRequiredTrueValidator.name} |`, () => {
    let targetForm: FormGroup;

    beforeEach(() => {
        targetForm = new FormGroup({
            control1: new FormControl(),
            control2: new FormControl(),
            control3: new FormControl()
        });
    });

    it('Should return validation error, if form does not have some control with "true" value', () => {
        // act
        const result = oneOrMoreRequiredTrueValidator(targetForm);

        // assert
        expect(result).toEqual({
            [ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR]: true
        });
    });

    it('Should return null, if form has some control with "true" value', () => {
        // arrange
        targetForm.setValue({
            control1: true,
            control2: false,
            control3: true
        });

        // act
        const result = oneOrMoreRequiredTrueValidator(targetForm);

        // assert
        expect(result).toEqual(null);
    });
});
