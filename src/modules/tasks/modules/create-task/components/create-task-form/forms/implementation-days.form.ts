import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import {
    oneOrMoreRequiredTrueValidator,
    ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR
} from './one-or-more-required-true.validator';

export class ImplementationDaysForm extends FormGroup {
    constructor() {
        const controls: { [controlName: string]: AbstractControl } = {
            single: new FormGroup(
                {
                    monday: new FormControl(false),
                    tuesday: new FormControl(false),
                    wednesday: new FormControl(false),
                    thursday: new FormControl(false),
                    friday: new FormControl(false),
                    saturday: new FormControl(false),
                    sunday: new FormControl(false)
                },
                [oneOrMoreRequiredTrueValidator]
            ),
            // todo revision
            combinations: new FormGroup({
                everyDay: new FormControl(false),
                weekdays: new FormControl(false),
                weekends: new FormControl(false)
            })
        };

        super(controls);
    }

    showOneOrMoreRequiredTrueError(): boolean {
        return this.touched && this.hasOneOrMoreRequiredTrueError();
    }

    hasOneOrMoreRequiredTrueError(): boolean {
        return this.get('single').hasError(
            ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR
        );
    }
}
