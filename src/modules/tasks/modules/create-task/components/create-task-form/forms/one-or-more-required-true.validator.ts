import { FormGroup, ValidationErrors } from '@angular/forms';

export const ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR =
    'oneOrMoreRequiredTrue';

export function oneOrMoreRequiredTrueValidator(
    form: FormGroup
): ValidationErrors | null {
    const formHasTrueValue = Object.entries(form.controls)
        .map(entity => entity[1].value)
        .reduce(
            (previousValue, currentValue) => previousValue || currentValue,
            false
        );

    return formHasTrueValue
        ? null
        : { [ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR]: true };
}
