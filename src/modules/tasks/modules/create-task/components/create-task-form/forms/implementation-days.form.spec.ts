import { FormControl, FormGroup } from '@angular/forms';
import { ImplementationDaysForm } from './implementation-days.form';
import { ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR } from './one-or-more-required-true.validator';

describe(`${ImplementationDaysForm.name} | `, () => {
    let form: ImplementationDaysForm;
    let singleControlEmptyValue: { [key: string]: boolean };

    beforeEach(() => {
        form = new ImplementationDaysForm();

        singleControlEmptyValue = {
            monday: false,
            tuesday: false,
            wednesday: false,
            thursday: false,
            friday: false,
            saturday: false,
            sunday: false
        };
    });

    it('Should extends FormGroup', () => {
        expect(form instanceof FormGroup).toBe(true);
    });

    describe('Controls | ', () => {
        describe('combinations | ', () => {
            let combinations: FormGroup;

            beforeEach(() => {
                combinations = form.get('combinations') as FormGroup;
            });

            it('Should extends FormGroup', () => {
                expect(combinations instanceof FormGroup).toBe(true);
            });

            it('everyDay', () => {
                // arrange
                const control = combinations.get('everyDay');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('weekdays', () => {
                // arrange
                const control = combinations.get('weekdays');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('weekends', () => {
                // arrange
                const control = combinations.get('weekends');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });
        });

        describe('single | ', () => {
            let single: FormGroup;

            beforeEach(() => {
                single = form.get('single') as FormGroup;
            });

            describe('Validation |', () => {
                it('Should has oneOrMoreRequiredTrueValidator', () => {
                    // act
                    single.setValue(singleControlEmptyValue);

                    // assert
                    expect(
                        single.hasError(
                            ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR
                        )
                    ).toBe(true, 'true');

                    // act
                    single.setValue({
                        ...singleControlEmptyValue,
                        monday: true
                    });

                    // assert
                    expect(
                        single.hasError(
                            ONE_OR_MORE_REQUIRED_TRUE_VALIDATION_ERROR
                        )
                    ).toBe(false, 'false');
                });
            });

            it('Should extends FormGroup', () => {
                expect(single instanceof FormGroup).toBe(true);
            });

            it('monday', () => {
                // arrange
                const control = single.get('monday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('tuesday', () => {
                // arrange
                const control = single.get('tuesday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('wednesday', () => {
                // arrange
                const control = single.get('wednesday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('thursday', () => {
                // arrange
                const control = single.get('thursday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('friday', () => {
                // arrange
                const control = single.get('friday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('saturday', () => {
                // arrange
                const control = single.get('saturday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });

            it('sunday', () => {
                // arrange
                const control = single.get('sunday');

                // assert
                expect(control.value).toBe(false);
                expect(control instanceof FormControl).toBe(true);
            });
        });
    });

    describe('#showOneOrMoreRequiredTrueError | ', () => {
        let singleControl: FormControl;

        beforeEach(() => {
            singleControl = form.get('single') as FormControl;
        });

        it('Should return "true", if form has touched and form has oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue(singleControlEmptyValue);
            form.markAsTouched();

            // assert
            expect(form.showOneOrMoreRequiredTrueError()).toBe(true);
        });

        it('Should return "false", if form has touched and form does not have oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue({
                ...singleControlEmptyValue,
                monday: true
            });
            form.markAsTouched();

            // assert
            expect(form.showOneOrMoreRequiredTrueError()).toBe(false);
        });

        it('Should return "false", if form has untouched and form has oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue(singleControlEmptyValue);
            form.markAsUntouched();

            // assert
            expect(form.showOneOrMoreRequiredTrueError()).toBe(false);
        });

        it('Should return "false", if form has untouched and form does not have oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue({
                ...singleControlEmptyValue,
                monday: true
            });
            form.markAsUntouched();

            // assert
            expect(form.showOneOrMoreRequiredTrueError()).toBe(false);
        });
    });

    describe('#hasOneOrMoreRequiredTrueError | ', () => {
        let singleControl: FormControl;

        beforeEach(() => {
            singleControl = form.get('single') as FormControl;
        });

        it('Should return "true", if form has oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue(singleControlEmptyValue);

            // assert
            expect(form.hasOneOrMoreRequiredTrueError()).toBe(true);
        });

        it('Should return "false", if form does not have oneOrMoreRequiredTrue error', () => {
            // act
            singleControl.setValue({
                ...singleControlEmptyValue,
                monday: true
            });

            // assert
            expect(form.hasOneOrMoreRequiredTrueError()).toBe(false);
        });
    });
});
