import { FormControl, FormGroup } from '@angular/forms';
import { CreateTaskForm } from './create-task.form';
import { ImplementationDaysForm } from './implementation-days.form';

describe(`${CreateTaskForm.name} | `, () => {
    let form: CreateTaskForm;

    beforeEach(() => {
        form = new CreateTaskForm();
    });

    it('Should extends FormGroup', () => {
        expect(form instanceof FormGroup).toBe(true);
    });

    describe('#hasTitleError | ', () => {
        it('Should return "true", if #title is empty', () => {
            // act
            form.get('title').setValue(null);

            // assert
            expect(form.hasTitleError()).toBe(true);
        });

        it('Should return "true", if #title is fill', () => {
            // act
            form.get('title').setValue('value');

            // assert
            expect(form.hasTitleError()).toBe(false);
        });
    });

    describe('#showTitleError | ', () => {
        it('Should return "true", if #title is empty and control is touched', () => {
            // act
            form.get('title').setValue(null);
            form.get('title').markAsTouched();

            // assert
            expect(form.showTitleError()).toBe(true);
        });

        it('Should return "false", if #title is empty and control does not touched', () => {
            // act
            form.get('title').setValue(null);
            form.get('title').markAsUntouched();

            // assert
            expect(form.showTitleError()).toBe(false);
        });

        it('Should return "false", if #title is fill and control does not touched', () => {
            // act
            form.get('title').setValue('value');
            form.get('title').markAsUntouched();

            // assert
            expect(form.showTitleError()).toBe(false);
        });

        it('Should return "false", if #title is fill and control is touched', () => {
            // act
            form.get('title').setValue('value');
            form.get('title').markAsTouched();

            // assert
            expect(form.showTitleError()).toBe(false);
        });
    });

    describe('Should be created controls | ', () => {
        describe('title | ', () => {
            it('Initialize', () => {
                expect(form.get('title') instanceof FormControl).toBe(true);
            });

            it('Validation', () => {
                // arrange
                const control = form.get('title');

                // act
                control.setValue('title');

                // assert
                expect(control.hasError('required')).toBe(false);

                // act
                control.setValue(null);

                // assert
                expect(control.hasError('required')).toBe(true);
            });
        });

        it('description', () => {
            expect(form.get('description') instanceof FormControl).toBe(true);
        });

        it('deadline', () => {
            expect(form.get('deadline') instanceof FormControl).toBe(true);
        });

        it('implementationDays', () => {
            expect(
                form.get('implementationDays') instanceof ImplementationDaysForm
            ).toBe(true);
        });
    });
});
