import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'lima-create-task-page',
    templateUrl: './create-task-page.component.html',
    styleUrls: ['./create-task-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateTaskPageComponent {}
