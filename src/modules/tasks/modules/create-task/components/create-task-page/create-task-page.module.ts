import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateTaskPageComponent } from './create-task-page.component';
import { CreateTaskFormModule } from '../create-task-form/create-task-form.module';

@NgModule({
    imports: [CommonModule, CreateTaskFormModule],
    declarations: [CreateTaskPageComponent],
    exports: [CreateTaskPageComponent]
})
export class CreateTaskPageModule {}
