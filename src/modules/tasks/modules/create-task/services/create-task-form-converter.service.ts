import { Injectable } from '@angular/core';
import { ITaskCreateUpdateForm } from 'modules/tasks/modules/create-task/models/i-task-create-update-form';
import { ITaskCreateUpdateDto } from 'modules/tasks/modules/create-task/models/i-task-create-update-dto';
import { DaysConverter } from 'modules/tasks/modules/create-task/models/day';

@Injectable({
    providedIn: 'root' // import in some module
})
export class CreateTaskFormConverterService {
    /**
     * todo: #issue
     * Сравнить статические классы и сервисы конвертаций:
     *      плюсы и минусы
     */

    convertFormToDto(form: ITaskCreateUpdateForm): ITaskCreateUpdateDto {
        const deadline = (form.deadline && form.deadline.toISOString()) || null;
        const implementationDays = DaysConverter.toArray(
            form.implementationDays
        );

        return {
            title: form.title,
            description: form.description,
            implementationDays,
            deadline
        };
    }

    convertDtoToForm(dto: ITaskCreateUpdateDto): ITaskCreateUpdateForm {
        const deadline = new Date(dto.deadline);
        const implementationDays = DaysConverter.toForm(dto.implementationDays);

        return {
            title: dto.title,
            description: dto.description,
            implementationDays,
            deadline
        };
    }
}
