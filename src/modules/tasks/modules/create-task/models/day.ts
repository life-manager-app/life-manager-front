export interface ImplementationDays {
    single: {
        monday: boolean;
        tuesday: boolean;
        wednesday: boolean;
        thursday: boolean;
        friday: boolean;
        saturday: boolean;
        sunday: boolean;
    };
    // todo: need revision
    // combinations: {
    //     everyDay: boolean;
    //     weekdays: boolean;
    //     weekends: boolean;
    // };
}

export type Days = Day[];

export enum Day {
    Monday = 'Monday',
    Tuesday = 'Tuesday',
    Wednesday = 'Wednesday',
    Thursday = 'Thursday',
    Friday = 'Friday',
    Saturday = 'Saturday',
    Sunday = 'Sunday'
}

export enum DaysAbbreviation {
    Monday = 'Mon',
    Tuesday = 'Tue',
    Wednesday = 'Wed',
    Thursday = 'Thu',
    Friday = 'Fri',
    Saturday = 'Sat',
    Sunday = 'Sun'
}

export class DaysConverter {
    static toForm(days: Days): ImplementationDays {
        const result = {
            single: {
                monday: false,
                tuesday: false,
                wednesday: false,
                thursday: false,
                friday: false,
                saturday: false,
                sunday: false
            }
        };

        if (days.includes(Day.Monday)) {
            result.single.monday = true;
        }

        if (days.includes(Day.Tuesday)) {
            result.single.tuesday = true;
        }

        if (days.includes(Day.Wednesday)) {
            result.single.wednesday = true;
        }

        if (days.includes(Day.Thursday)) {
            result.single.thursday = true;
        }

        if (days.includes(Day.Friday)) {
            result.single.friday = true;
        }

        if (days.includes(Day.Saturday)) {
            result.single.saturday = true;
        }

        if (days.includes(Day.Sunday)) {
            result.single.sunday = true;
        }

        return result;
    }

    static toArray(days: ImplementationDays): Days {
        let result: Days = [];

        if (days.single.monday) {
            result.push(Day.Monday);
        }

        if (days.single.tuesday) {
            result.push(Day.Tuesday);
        }

        if (days.single.wednesday) {
            result.push(Day.Wednesday);
        }

        if (days.single.thursday) {
            result.push(Day.Thursday);
        }

        if (days.single.friday) {
            result.push(Day.Friday);
        }

        if (days.single.saturday) {
            result.push(Day.Saturday);
        }

        if (days.single.sunday) {
            result.push(Day.Sunday);
        }

        return result;
    }
}

// export class DaysConverter2 {
//     static init = {
//         single: {
//             monday: true,
//             tuesday: true,
//             wednesday: true,
//             thursday: true,
//             friday: true,
//             saturday: true,
//             sunday: true
//         },
//         combinations: {
//             everyDay: true,
//             weekdays: true,
//             weekends: true
//         }
//     };
//
//     static toForm(daysArray: Days): ImplementationDays {
//         const result: ImplementationDays = {
//             single: {
//                 monday: true,
//                 tuesday: true,
//                 wednesday: true,
//                 thursday: true,
//                 friday: true,
//                 saturday: true,
//                 sunday: true
//             },
//             combinations: {
//                 everyDay: true,
//                 weekdays: true,
//                 weekends: true
//             }
//         };
//
//         if (daysArray.length === 7) {
//             return result;
//         } else {
//             result.combinations.everyDay = false;
//         }
//
//         if (!daysArray.includes(Day.Monday)) {
//             result.single.monday = false;
//             result.combinations.weekdays = false;
//         }
//
//         if (!daysArray.includes(Day.Tuesday)) {
//             result.single.tuesday = false;
//             result.combinations.weekdays = false;
//         }
//
//         if (!daysArray.includes(Day.Wednesday)) {
//             result.single.wednesday = false;
//             result.combinations.weekdays = false;
//         }
//
//         if (!daysArray.includes(Day.Thursday)) {
//             result.single.thursday = false;
//             result.combinations.weekdays = false;
//         }
//
//         if (!daysArray.includes(Day.Friday)) {
//             result.single.friday = false;
//             result.combinations.weekdays = false;
//         }
//
//         if (!daysArray.includes(Day.Saturday)) {
//             result.single.saturday = false;
//             result.combinations.weekends = false;
//         }
//
//         if (!daysArray.includes(Day.Sunday)) {
//             result.single.sunday = false;
//             result.combinations.weekends = false;
//         }
//
//         return this.normalize(result);
//         // return result;
//     }
//
//     // TODO does it work?
//     static toArray(days: ImplementationDays): Days {
//         // todo - посмотреть, что будет без нормализации
//         let normalisedDays = this.normalize(days);
//
//         let result: Days = [
//             Day.Monday,
//             Day.Tuesday,
//             Day.Wednesday,
//             Day.Thursday,
//             Day.Friday,
//             Day.Saturday,
//             Day.Sunday
//         ];
//
//         if (!normalisedDays.single.monday) {
//             result = result.filter(item => item !== Day.Monday);
//         }
//
//         if (!normalisedDays.single.tuesday) {
//             result = result.filter(item => item !== Day.Tuesday);
//         }
//
//         if (!normalisedDays.single.wednesday) {
//             result = result.filter(item => item !== Day.Wednesday);
//         }
//
//         if (!normalisedDays.single.thursday) {
//             result = result.filter(item => item !== Day.Thursday);
//         }
//
//         if (!normalisedDays.single.friday) {
//             result = result.filter(item => item !== Day.Friday);
//         }
//
//         if (!normalisedDays.single.saturday) {
//             result = result.filter(item => item !== Day.Saturday);
//         }
//
//         if (!normalisedDays.single.sunday) {
//             result = result.filter(item => item !== Day.Sunday);
//         }
//
//         return result;
//     }
//
//     // TODO - write description of this method
//     // todo test
//     static normalize(
//         implementationDays: ImplementationDays
//     ): ImplementationDays {
//         let result: ImplementationDays = { ...implementationDays };
//
//         if (implementationDays.combinations.everyDay) {
//             result = {
//                 single: {
//                     monday: true,
//                     tuesday: true,
//                     wednesday: true,
//                     thursday: true,
//                     friday: true,
//                     saturday: true,
//                     sunday: true
//                 },
//                 combinations: {
//                     everyDay: true,
//                     weekdays: true,
//                     weekends: true
//                 }
//             };
//         } else {
//             result = {
//                 single: {
//                     monday: false,
//                     tuesday: false,
//                     wednesday: false,
//                     thursday: false,
//                     friday: false,
//                     saturday: false,
//                     sunday: false
//                 },
//                 combinations: {
//                     everyDay: false,
//                     weekdays: false,
//                     weekends: false
//                 }
//             };
//         }
//
//         if (implementationDays.combinations.weekdays) {
//             result = {
//                 single: {
//                     monday: true,
//                     tuesday: true,
//                     wednesday: true,
//                     thursday: true,
//                     friday: true,
//                     saturday: result.single.saturday,
//                     sunday: result.single.sunday
//                 },
//                 combinations: {
//                     everyDay: result.combinations.everyDay,
//                     weekdays: true,
//                     weekends: result.combinations.weekends
//                 }
//             };
//         }
//
//         if (implementationDays.combinations.weekends) {
//             result = {
//                 single: {
//                     monday: result.single.monday,
//                     tuesday: result.single.tuesday,
//                     wednesday: result.single.wednesday,
//                     thursday: result.single.thursday,
//                     friday: result.single.friday,
//                     saturday: true,
//                     sunday: true
//                 },
//                 combinations: {
//                     everyDay: result.combinations.everyDay,
//                     weekdays: result.combinations.weekdays,
//                     weekends: true
//                 }
//             };
//         }
//
//         return result;
//     }
//
//     private static normalizeCombinations(
//         implementationDays: ImplementationDays
//     ): ImplementationDays {
//         let result = { ...implementationDays };
//
//         if (implementationDays.combinations.everyDay) {
//             result.combinations.weekdays = true;
//             result.combinations.weekends = true;
//         } else {
//             result.combinations.weekdays = false;
//             result.combinations.weekends = false;
//         }
//
//         return result;
//     }
// }
