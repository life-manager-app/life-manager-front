import { Days } from './day';
import { ISODateString } from 'shared/utils/common-types';

export interface ITaskCreateUpdateDto {
    title: string;
    description: string;
    implementationDays: Days;

    // предоставить пользователю выбрать продолжительность
    // (месяц, неделя или год и рассчитать эту дату самостоятельно, относительно даты создания таски)
    // или предложить выбрать конкретную дату самостоятельно
    deadline: ISODateString;
}
