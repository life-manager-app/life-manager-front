import { Day, DaysConverter } from './day';

describe('DaysConverter', () => {
    describe('#toForm | todo desc', () => {
        [
            {
                description: 'case 1',
                input: [
                    Day.Monday,
                    Day.Tuesday,
                    Day.Thursday,
                    Day.Friday,
                    Day.Sunday
                ],
                expected: {
                    single: {
                        monday: true,
                        tuesday: true,
                        wednesday: false,
                        thursday: true,
                        friday: true,
                        saturday: false,
                        sunday: true
                    }
                }
            },
            {
                description: 'case 2',
                input: [
                    Day.Monday,
                    Day.Tuesday,
                    Day.Wednesday,
                    Day.Thursday,
                    Day.Friday,
                    Day.Saturday,
                    Day.Sunday
                ],
                expected: {
                    single: {
                        monday: true,
                        tuesday: true,
                        wednesday: true,
                        thursday: true,
                        friday: true,
                        saturday: true,
                        sunday: true
                    }
                }
            },
            {
                description: 'case 3',
                input: [Day.Saturday, Day.Sunday],
                expected: {
                    single: {
                        monday: false,
                        tuesday: false,
                        wednesday: false,
                        thursday: false,
                        friday: false,
                        saturday: true,
                        sunday: true
                    }
                }
            }
        ].forEach(({ description, input, expected }) => {
            it(`${description}`, () => {
                expect(DaysConverter.toForm(input)).toEqual(expected);
            });
        });
    });

    describe('#toArray | todo desc', () => {
        [
            {
                description: 'case 1',
                input: {
                    single: {
                        monday: true,
                        tuesday: true,
                        wednesday: false,
                        thursday: true,
                        friday: true,
                        saturday: false,
                        sunday: true
                    }
                },
                expected: [
                    Day.Monday,
                    Day.Tuesday,
                    Day.Thursday,
                    Day.Friday,
                    Day.Sunday
                ]
            },
            {
                description: 'case 2',
                input: {
                    single: {
                        monday: true,
                        tuesday: true,
                        wednesday: true,
                        thursday: true,
                        friday: true,
                        saturday: true,
                        sunday: true
                    }
                },
                expected: [
                    Day.Monday,
                    Day.Tuesday,
                    Day.Wednesday,
                    Day.Thursday,
                    Day.Friday,
                    Day.Saturday,
                    Day.Sunday
                ]
            },
            {
                description: 'case 3',
                input: {
                    single: {
                        monday: false,
                        tuesday: false,
                        wednesday: false,
                        thursday: false,
                        friday: false,
                        saturday: true,
                        sunday: true
                    }
                },
                expected: [Day.Saturday, Day.Sunday]
            }
        ].forEach(({ description, input, expected }) => {
            it(`${description}`, () => {
                expect(DaysConverter.toArray(input)).toEqual(expected);
            });
        });
    });

    xit('', () => {
        expect(true).toBe(false);
    });
});

// describe('DaysConverter2', () => {
//     /**
//      * todo: need revision
//      *
//      * */
//
//     // TODO - finish tests and come up with descriptions
//     xdescribe('#toForm | Convert days array to form object', () => {
//         [
//             {
//                 description: 'case 1',
//                 input: [
//                     Day.Monday,
//                     Day.Tuesday,
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ],
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 2',
//                 input: [
//                     Day.Tuesday,
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ],
//                 expected: {
//                     single: {
//                         monday: false,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: false,
//                         weekdays: false,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 3',
//                 input: [
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ],
//                 expected: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: false,
//                         weekdays: false,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 4',
//                 input: [
//                     Day.Monday,
//                     Day.Tuesday,
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ],
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 5',
//                 input: [
//                     Day.Monday,
//                     Day.Tuesday,
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ],
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             }
//         ].forEach(({ description, input, expected }) => {
//             it(`${description}`, () => {
//                 expect(DaysConverter2.toForm(input)).toEqual(expected);
//             });
//         });
//     });
//
//     xdescribe('#toArray | Convert form(?) object to days array', () => {
//         [
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: [
//                     Day.Monday,
//                     Day.Tuesday,
//                     Day.Wednesday,
//                     Day.Thursday,
//                     Day.Friday,
//                     Day.Saturday,
//                     Day.Sunday
//                 ]
//             }
//         ].forEach(({ description, input, expected }) => {
//             it(`${description}`, () => {
//                 expect(DaysConverter2.toArray(input)).toEqual(expected);
//             });
//         });
//     });
//
//     xdescribe('#normalize | normilaze', () => {
//         [
//             {
//                 description: 'case 0',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             },
//             {
//                 description: 'case 1',
//                 input: {
//                     single: {
//                         monday: false,
//                         tuesday: false,
//                         wednesday: false,
//                         thursday: false,
//                         friday: false,
//                         saturday: false,
//                         sunday: false
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 },
//                 expected: {
//                     single: {
//                         monday: true,
//                         tuesday: true,
//                         wednesday: true,
//                         thursday: true,
//                         friday: true,
//                         saturday: true,
//                         sunday: true
//                     },
//                     combinations: {
//                         everyDay: true,
//                         weekdays: true,
//                         weekends: true
//                     }
//                 }
//             }
//         ].forEach(({ description, input, expected }) => {
//             it(`${description}`, () => {
//                 expect(DaysConverter2.normalize(input)).toEqual(expected);
//             });
//         });
//     });
// });
