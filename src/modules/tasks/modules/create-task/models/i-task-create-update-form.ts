export interface IImplementationDays {
    single: {
        monday: boolean;
        tuesday: boolean;
        wednesday: boolean;
        thursday: boolean;
        friday: boolean;
        saturday: boolean;
        sunday: boolean;
    };
}

export interface ITaskCreateUpdateForm {
    title: string;
    description: string;
    implementationDays: IImplementationDays;
    deadline: Date;
}
