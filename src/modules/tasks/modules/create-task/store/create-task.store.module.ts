import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { StoreFeatures } from 'modules/app/store/app.store';
import { CreateTaskEffects } from 'modules/tasks/modules/create-task/store/create-task.effects';
import { createTaskReducer } from './create-task.reducer';

@NgModule({
    imports: [
        StoreModule.forFeature(StoreFeatures.CreateTask, createTaskReducer),
        EffectsModule.forFeature([CreateTaskEffects])
    ]
})
export class CreateTaskStoreModule {}
