import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
    CreateTask,
    CreateTaskActionTypes,
    CreateTaskFail,
    CreateTaskSuccess
} from './create-task.actions';
import { TasksApiService } from '../../../services/tasks-api.service';

@Injectable()
export class CreateTaskEffects {
    constructor(
        private readonly action$: Actions,
        private readonly api: TasksApiService
    ) {}

    @Effect()
    createTask$ = this.action$.pipe(
        tap(a => console.log(' -#-#- a  = ', a)),
        ofType(CreateTaskActionTypes.CreateTask),
        switchMap((action: CreateTask) => this.api.createTask$(action.payload)),
        // todo need result?
        map(result => new CreateTaskSuccess(result)),
        catchError(error => of(new CreateTaskFail(error)))
    );
}
