import { Action } from '@ngrx/store';
import { ITaskCreateUpdateDto } from 'modules/tasks/modules/create-task/models/i-task-create-update-dto';
import { ServerError } from 'shared/utils/common-types';

export enum CreateTaskActionTypes {
    CreateTask = '[Create Task] Create Task',
    CreateTaskSuccess = '[Create Task] Create Task Success',
    CreateTaskFail = '[Create Task] Create Task Fail'
}

export class CreateTask implements Action {
    readonly type = CreateTaskActionTypes.CreateTask;

    constructor(public payload: ITaskCreateUpdateDto) {}
}

export class CreateTaskSuccess implements Action {
    readonly type = CreateTaskActionTypes.CreateTaskSuccess;

    constructor(public payload: any) {}
}

export class CreateTaskFail implements Action {
    readonly type = CreateTaskActionTypes.CreateTaskFail;

    constructor(public payload: ServerError) {}
}

export type CreateTaskAction = CreateTask | CreateTaskSuccess | CreateTaskFail;
