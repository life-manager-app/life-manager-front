import { createFeatureSelector } from '@ngrx/store';
import { StoreFeatures } from 'modules/app/store/app.store';
import { CreateTaskState } from './create-task.reducer';

const getCreateTaskFeatureState = createFeatureSelector<CreateTaskState>(
    StoreFeatures.CreateTask
);
