import { AppState, StoreFeatures } from 'modules/app/store/app.store';
import { CreateTaskAction, CreateTaskActionTypes } from './create-task.actions';
import { ServerError } from 'shared/utils/common-types';

export interface CreateTaskState {
    loading: boolean;
    loadingError: ServerError;
}

const initialState: CreateTaskState = {
    loading: false,
    loadingError: null
};

export interface State extends AppState {
    [StoreFeatures.CreateTask]: CreateTaskState;
}

export function createTaskReducer(
    state = initialState,
    action: CreateTaskAction
) {
    switch (action.type) {
        case CreateTaskActionTypes.CreateTask: {
            return {
                ...state,
                loading: true
            };
        }

        case CreateTaskActionTypes.CreateTaskSuccess: {
            return {
                ...state,
                loading: false,
                loadingError: null
            };
        }

        case CreateTaskActionTypes.CreateTaskFail: {
            return {
                ...state,
                loading: false,
                loadingError: action.payload
            };
        }

        default:
            return state;
    }
}
