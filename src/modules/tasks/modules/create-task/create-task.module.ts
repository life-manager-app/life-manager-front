import { NgModule } from "@angular/core";
import { CreateTaskPageModule } from "./components/create-task-page/create-task-page.module";
import { CreateTaskStoreModule } from 'modules/tasks/modules/create-task/store/create-task.store.module';

@NgModule({
    imports: [CreateTaskStoreModule, CreateTaskPageModule]
})
export class CreateTaskModule {}
