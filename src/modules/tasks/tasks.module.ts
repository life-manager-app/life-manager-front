import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksListModule } from './modules/tasks-list/tasks-list.module';
import { TaskDetailsModule } from './modules/task-details/task-details.module';
import { CreateTaskModule } from './modules/create-task/create-task.module';
import { TasksApiService } from './services/tasks-api.service';

@NgModule({
    imports: [
        CommonModule,
        TasksListModule,
        TaskDetailsModule,
        CreateTaskModule,
        TasksRoutingModule
    ],
    providers: [TasksApiService]
})
export class TasksModule {}
