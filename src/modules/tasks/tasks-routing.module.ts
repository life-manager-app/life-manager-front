import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksListPageComponent } from './modules/tasks-list/components/tasks-list-page/tasks-list-page.component';
import { TaskDetailsPageComponent } from './modules/task-details/components/task-details-page/task-details-page.component';
import { CreateTaskPageComponent } from './modules/create-task/components/create-task-page/create-task-page.component';

const routes: Routes = [
    {
        path: '',
        component: TasksListPageComponent
    },
    {
        path: 'create',
        component: CreateTaskPageComponent
    },
    {
        path: ':taskId',
        component: TaskDetailsPageComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TasksRoutingModule {}
