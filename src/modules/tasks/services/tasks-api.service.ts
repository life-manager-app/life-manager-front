import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { ITaskCreateUpdateDto } from 'modules/tasks/modules/create-task/models/i-task-create-update-dto';
import { TasksListItemDto } from 'modules/tasks/modules/tasks-list/models/tasks-list-item.dto';

// todo provide in FeatureModule (find article)
@Injectable()
export class TasksApiService {
    constructor(private readonly http: HttpClient) {}

    private _tasks = new BehaviorSubject<TasksListItemDto[]>([]);

    // todo typing
    createTask$(task: ITaskCreateUpdateDto): Observable<any> {
        // return this.http.post<any>('url', task);

        this.addNewTasks(task);
        return of({});
    }

    // --------------- mocks
    private addNewTasks(task: ITaskCreateUpdateDto) {
        const tasks = this._tasks.value;
        const genId = String(Math.round(Math.random() * 10000000));
        const listItem: TasksListItemDto = {
            id: genId,
            title: task.title,
            description: task.description
        };

        tasks.push(listItem);
        this._tasks.next(tasks);

        console.log(' -#-#- this._tasks.value  = ', this._tasks.value);
    }
}
