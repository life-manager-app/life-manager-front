import { Params } from '@angular/router';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import { ActionReducerMap } from '@ngrx/store';
import { StoreFeatures } from '../app.store';

export interface RouterState {
    url: string;
    queryParams: Params;
    params: Params;
}

export interface State {
    [StoreFeatures.RouterState]: RouterReducerState<RouterState>
}

export const reducers: ActionReducerMap<State> = {
    [StoreFeatures.RouterState]: routerReducer
};
