import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { RouterStateSerializer } from '@ngrx/router-store';
import { RouterState } from './router.reducer';

export class CustomRouterSerializer extends RouterStateSerializer<RouterState> {
    serialize(routerState: RouterStateSnapshot): RouterState {
        const {url} = routerState;
        const {queryParams} = routerState.root;

        let state: ActivatedRouteSnapshot = routerState.root;

        // todo !
        while (state.firstChild) {
            state = state.firstChild;
        }

        const {params} = routerState.root;

        return {url, queryParams, params};
    }
}

// todo - более жоский сериалайзер:
// src = https://medium.com/simars/ngrx-router-store-reduce-select-route-params-6baff607dd9
