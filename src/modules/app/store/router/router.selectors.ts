import { createFeatureSelector } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';
import { RouterState } from './router.reducer';
import { StoreFeatures } from '../app.store';

export const getRouterState = createFeatureSelector<RouterReducerState<RouterState>>(StoreFeatures.RouterState);
