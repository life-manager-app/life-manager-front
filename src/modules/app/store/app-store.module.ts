import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { CustomRouterSerializer, reducers } from './router';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../../../environments/environment';

@NgModule({
    declarations: [],
    imports: [
        // todo - а если захочу еще больше редьюсеров?
        StoreModule.forRoot(reducers),
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot({serializer: CustomRouterSerializer}),
        StoreDevtoolsModule.instrument(
            {
                maxAge: 25,
                logOnly: environment.production
            }
        )
    ]
})
export class AppStoreModule { }
