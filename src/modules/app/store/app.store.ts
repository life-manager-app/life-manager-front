// todo rename ? FeatureStoreNames
export enum StoreFeatures {
    RouterState = 'RouterState',
    CreateTask = 'CreateTask',
    TaskDetails = 'TaskDetails',
    TasksList = 'TasksList'
}

export interface AppState {}

// export type AppState = {
//     [K in StoreFeatures]: any
// }

// const a: AppState = {tasksList: {}, kek: {}};
