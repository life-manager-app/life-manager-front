import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(
        private cookieService: CookieService,
        private http: HttpClient
    ) {
        this.init();
    }

    private init() {
        const userData = { username: 'alexplusm', password: 'password' };
        // http.post('/api/user', userData).subscribe();

        this.http.post<{ id: string }>('/api/login', userData).subscribe(
            data => {
                this.cookieService.set('limaUserId', data.id as string);

                console.log(
                    ' -#-#- cookieService  = ',
                    this.cookieService.getAll()
                );
            },
            error => console.log(' -#-#- error  = ', error)
        );

        // setTimeout(() => {
        //     this.testRequest();
        // }, 400);
    }

    private testRequest() {
        const data = { data: 'data' };

        this.http
            .post('/api/task', data)
            .subscribe(
                d => console.log(' -#-#- d  = ', d),
                e => console.log(' -#-#- e  = ', e)
            );
    }
}
